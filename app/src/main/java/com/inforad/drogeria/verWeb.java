package com.inforad.drogeria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;

public class verWeb extends AppCompatActivity {

    //Datos personales Pedro Francisco Diaz Siccha

    private WebView wevView;
    private Button btnSiguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ver_web);
        Iniciar();
        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(verWeb.this, queja.class);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        wevView = findViewById(R.id.wvWeb);
        wevView.loadUrl("https://pruebarad.000webhostapp.com/");
        btnSiguiente = findViewById(R.id.bSiguiente);
    }
}