package com.inforad.drogeria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class verGps extends AppCompatActivity {

    //Datos personales Pedro Francisco Diaz Siccha

     private  WebView gpsView;
     private Button btnSiguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ver_gps);
        Iniciar();

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(verGps.this, verWeb.class);
                startActivity(intent);
            }
        });

    }

    private void Iniciar() {
        gpsView = findViewById(R.id.WebView);
        gpsView.getSettings().setJavaScriptEnabled(true);
        //webView.setWebViewClient(new WebViewClient());
        gpsView.loadUrl("https://www.google.com.pe/maps/place/Farmacia+Universal/@-12.0449231,-77.0490933,15z/data=!4m8!1m2!2m1!1sfarmacia!3m4!1s0x9105c8cc68c0ab35:0x2f13ee788112e384!8m2!3d-12.0449231!4d-77.0403386?hl=es-419");
        btnSiguiente = findViewById(R.id.bSiguiente);
    }
}