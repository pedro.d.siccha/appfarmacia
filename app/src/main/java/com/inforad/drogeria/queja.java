package com.inforad.drogeria;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.inforad.drogeria.OpenHelper.SQLite_OpenHelper;
import com.inforad.drogeria.utilidades.FileUtilidades;
import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class queja extends AppCompatActivity {

    //Datos personales Pedro Francisco Diaz Siccha

    private CircleImageView civProducto;
    private EditText inputAsunto, inputDescripcion;
    private Button btnGuardar;
    private final int GALLERY_REQUEST = 1;
    private File mImageFile;
    private SQLite_OpenHelper helper;
    private String urlImagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_queja);
        Iniciar();

        civProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirGaleria();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(queja.this, urlImagen, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(queja.this, miUbicacion.class);
                intent.putExtra("urlImagen", urlImagen);
                intent.putExtra("asunto", inputAsunto.getText().toString());
                intent.putExtra("descripcion", inputDescripcion.getText().toString());
                startActivity(intent);

            }
        });

    }

    private void Iniciar() {
        civProducto = findViewById(R.id.civImagen);
        inputAsunto = findViewById(R.id.etAsunto);
        inputDescripcion = findViewById(R.id.etDescripcion);
        btnGuardar = findViewById(R.id.bGuardar);

    }

    private void abrirGaleria(){
        Intent GaleriaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        GaleriaIntent.setType("image/*");
        startActivityForResult(GaleriaIntent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            try {
                mImageFile = FileUtilidades.from(this, data.getData());
                civProducto.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
                urlImagen = mImageFile.getPath();
            }catch (Exception e){
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

}