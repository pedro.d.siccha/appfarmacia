package com.inforad.drogeria.OpenHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SQLite_OpenHelper extends SQLiteOpenHelper {


    public SQLite_OpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "create table queja(_ID integer primary key autoincrement, Asunto text, Descripcion text, Imagen blob, double lalitud, double longitud);";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //Abrir la base de datos
    public void abrir(){
        this.getWritableDatabase();
    }

    //Cerrar la base de datos
    public void cerrar(){
        this.close();
    }

    public Boolean insertarRegistro(String x, String asunto, String descripcion, double latitud, double longitud){
        abrir();
        try {
            FileInputStream fs = new FileInputStream(x);
            byte[] imgByte = new byte[fs.available()];
            fs.read(imgByte);
            ContentValues valores = new ContentValues();
            valores.put("Asunto", asunto);
            valores.put("Descripcion", descripcion);
            valores.put("Imagen", imgByte);
            valores.put("latitud", latitud);
            valores.put("longitud", longitud);
            this.getWritableDatabase().insert("queja", null, valores);
            close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
